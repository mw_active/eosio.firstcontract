# EOSIO / EOSIO.cdt Tutorial #

## Deploying your first Smart Contract with eosio and eosio.cdt
Please find the blog post here. This repo contains the source required to get started.

### Dependencies ###
[eosio](https://github.com/EOSIO/eos)<br>
[eosio.cdt](https://github.com/EOSIO/eosio.cdt)

### Contact ###

* Repo owner: Walker / Mistywest
* [Mistywest](https://mistywest.com/)
<br><br>
![](https://mistywest.com/wp-content/uploads/2015/11/logo_sticky.png)
<br>
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
